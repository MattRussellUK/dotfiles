# Ubuntu packages
# ###############

# grep -v -e '^#' package-list.txt | sudo xargs apt-get install -y

# CLI core:

git
tree
mercurial
build-essential
unrar
cloc
daemon
httpie
dtrx
screen
tmux
mosh
cowsay
htop
glances
dos2unix
members
ag

# CLI other:

smem
sysstat
cntlm
openjdk-8-source
openjdk-7-source
zsh
fish
#scala  use scala-lang packages instead, more up-to-date
#scala-doc 
scala-mode-el
emacs-goodies-el
haskell-mode
haskell-platform
finger
echoping
vagrant
virtualbox
docker.io
postgresql
ghc
default-jdk
maven
xvfb
npm
ansible
python-pygments
python3-venv
python3-tk
python-pip
virtualenv
virtualenvwrapper
irssi
youtube-dl
coffeescript
texlive-full
rubber
bpython
ipython
ipython-notebook
ipython3
ipython3-notebook
awscli
ec2-api-tools
asciinema
vpnc
libpython3.4-dev
python-dev

# Desktop apps:

proofgeneral
coqide
emacs
gitg
gitk
libgnome2-bin
shutter
mplayer2
xsel
stellarium
celestia-gnome
xchat
ubuntu-restricted-extras
meld
xclip
unity-tweak-tool
indicator-multiload
wireshark
gimp
compizconfig-settings-manager
compiz-plugins
rdesktop
dconf-editor
bless
monodevelop
network-manager-vpnc
jmeter
jmeter-http
jmeter-apidoc
gedit-plugins
