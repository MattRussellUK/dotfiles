#!/bin/bash
set -e
set -u
cd "$(dirname "$0")"

symlink-file() {
  echo "Installing ~/$1"
  ln --force -s "$(readlink -f $1)" ~/"$1"
}

run-echo() {
  echo "$@"
  "$@"
}

symlink-file .bashrc
symlink-file .bash_aliases
symlink-file .emacs
symlink-file .gitconfig
symlink-file .hgrc
symlink-file .inputrc
symlink-file .nanorc
symlink-file .npmrc

echo "Installing ~/.config/gtk-3.0/gtk.css"
mkdir -p ~/.config/gtk-3.0
ln --force -s "$(readlink -f .config/gtk-3.0/gtk.css)" ~/.config/gtk-3.0/gtk.css

echo "Installing ~/.mash/mashrc"
mkdir -p ~/.mash
ln --force -s "$(readlink -f .mash/mashrc)" ~/.mash/mashrc

run-echo mkdir -p ~/.npm-packages
run-echo mkdir -p ~/.virtualenvs

run-echo gsettings set org.gnome.gedit.preferences.editor create-backup-copy false
run-echo gsettings set org.gnome.gedit.preferences.editor display-line-numbers true
run-echo gsettings set org.gnome.gedit.preferences.editor display-right-margin true
run-echo gsettings set org.gnome.gedit.preferences.editor right-margin-position 120
run-echo gsettings set org.gnome.gedit.preferences.ui statusbar-visible true
run-echo gsettings set org.gnome.gedit.preferences.editor insert-spaces true
run-echo gsettings set org.gnome.gedit.preferences.editor tabs-size 4
run-echo gsettings set com.canonical.indicator.datetime show-date true
run-echo gsettings set com.canonical.indicator.datetime show-day true
run-echo gsettings set com.canonical.indicator.power show-time true
run-echo gsettings set com.canonical.indicator.power show-percentage true
run-echo gsettings set com.ubuntu.sound allow-amplified-volume true

# Disable shortcuts which collide with Eclipse duplicate line binding
run-echo gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-up "['']"
run-echo gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-down "['']"

# Disable Unity search spam
run-echo gsettings set com.canonical.Unity.Lenses disabled-scopes "['more_suggestions-amazon.scope', 'more_suggestions-u1ms.scope', 'more_suggestions-populartracks.scope', 'music-musicstore.scope', 'more_suggestions-ebay.scope', 'more_suggestions-ubuntushop.scope', 'more_suggestions-skimlinks.scope']"

# Turn off HUD shortcut (normally bound to ALT)
run-echo gsettings set org.compiz.integrated show-hud '[]'

# sudo ln -s `which nodejs` /usr/bin/node
