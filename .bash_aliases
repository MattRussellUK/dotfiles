alias lr='ls -lctra'
alias lf='ls -ctr | tail -1'

alias g=git
alias gs='git status'
alias gd='git diff'
alias gdc='git diff --cached'
alias gl='git log'
alias glg='git log --graph'
alias glog='git log'
alias gf='git fetch'
alias gaa='git add -A .'
alias gwip='git add -A . && git commit -a -m "wip"'

alias dtrx='dtrx -n'

alias hs='hg status'
alias hd='hg diff'

cless() {
  heads=${@:1:$((${#@} - 1))}
  tail=${@:${#@}}
  pygmentize -f terminal -g $tail | /usr/bin/less -R $heads
}

detach() {
  nohup "$@" >&/dev/null &
}

alias go=gnome-open
alias vag=vagrant

alias mfz='mplayer -fs -zoom'

LINUS=~/.dotfiles/linus_bird.jpg

git() {
  /usr/bin/git "$@" || (timeout 2 display "$LINUS" &>/dev/null &)
}

gitsed() {
  git grep -l "$1" | xargs sed -i "s/$1/$2/g"
}

# From: http://daniele.livejournal.com/76011.html

function up()
{
    dir=""
    if [ -z "$1" ]; then
        dir=..
    elif [[ $1 =~ ^[0-9]+$ ]]; then
        x=0
        while [ $x -lt ${1:-1} ]; do
            dir=${dir}../
            x=$(($x+1))
        done
    else
        dir=${PWD%/$1/*}/$1
    fi
    cd "$dir";
}

function upstr()
{
    echo "$(up "$1" && pwd)";
}

alias copy='xclip -selection c'
alias paste='xclip -selection clipboard -o'
