;; Octave support

(autoload 'octave-mode "octave-mod" nil t)
(setq auto-mode-alist
      (cons '("\\.m$" . octave-mode) auto-mode-alist))

;; Scrolling

(setq scroll-step 1) ;; keyboard scroll one line at a time
(setq scroll-conservatively 10000)
(setq scroll-error-top-bottom t) ;; Page Up / Down go to top/bottom of buffer

;; Misc

(delete-selection-mode 1) ;; Replace active region just by typing text

(load-theme 'misterioso)

;; Cruft file suppression

(setq backup-directory-alist `(("." . "~/.emacs-saves")))
(setq backup-by-copying t)
(setq create-lockfiles nil)
(setq auto-save-file-name-transforms ;; Put autosave files (#blah#) in a temp dir
      `((".*" ,temporary-file-directory t)))

;; Extra package repos

(require 'package)
(add-to-list 'package-archives 
    '("marmalade" .
      "http://marmalade-repo.org/packages/"))
(package-initialize)

(when (>= emacs-major-version 24)
  (require 'package)
  (package-initialize)
  (add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/") t))

;; Server mode

(server-start)

;; goto-last-change

(autoload 'goto-last-change "goto-last-change"
  "Set point to the position of the last change." t)
(global-set-key "\C-q" 'goto-last-change)

;; Kill buffer
(defun kill-this-buffer-volatile ()
    "Kill current buffer, even if it has been modified."
    (interactive)
    (set-buffer-modified-p nil)
    (kill-this-buffer))

(global-set-key (kbd "C-x k") 'kill-this-buffer-volatile)

;; Bracket matching
(show-paren-mode 1) ; turn on paren match highlighting

;; Line numbers in margin
(global-linum-mode 1) 

;; Show column position:

(column-number-mode 1)

;; Recently-used files
(recentf-mode 1)

;; show print margin (package fill-column-indicator)
;; (add-hook 'after-change-major-mode-hook 'fci-mode)

(setq-default fill-column 92)

;; automatically reload all buffers if they change underfoot
(global-auto-revert-mode t)


(autoload 'markdown-mode "markdown-mode"
   "Major mode for editing Markdown files" t)
;;(add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

;; Follow symlinks to version controlled files
(setq vc-follow-symlinks t)
